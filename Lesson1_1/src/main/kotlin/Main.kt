fun main(args: Array<String>) {
    val myFirstObj:TestObj1 = TestObj1("Test1")
    var testObj:TestObj2? = null

    testObj = TestObj2("test2")

//    testObj.testCount = 3

    if (myFirstObj is A) {
        println(myFirstObj.testA())
    }
    if (testObj != null) {
        println(testObj.age)
    }

    println(testObj?.age)

    val r = testObj?.let {
        it.age
    }

    println("r >> $r")

    println(myFirstObj.addAgeAndPrint(34, ::toConsole))
    println(myFirstObj.addAgeAndPrint(44, fun(g:Int):String{ return "<< $g >>"}))
    println(myFirstObj.addAgeAndPrint(24) { d ->
        return@addAgeAndPrint "<<< $d <<<"
    })
    println(testObj.getSqAge())
}

fun toConsole(i: Int): String {
    return ">> $i <<"
}