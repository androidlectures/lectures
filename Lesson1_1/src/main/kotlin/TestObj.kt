abstract class TestObj(var tesVal:String?, var age:Int = 4) {
    abstract fun testCount():Int
}

class TestObj1(testObj: String) : TestObj(testObj), A, B, C {
    override fun testCount(): Int {
        return 1
    }

    fun addAgeAndPrint(addAdg: Int, toStr: (Int)->String):String {
        age += addAdg
        return toStr(age)
    }

    override fun testB(): Int {
        return 90
    }

    override fun testA(): String {
        var test = ""
        if (age%2 == 0) {
            test = super<C>.testA()
        } else {
            test = super<A>.testA()
        }
        return test
    }
}

class TestObj2(testObj: String) : TestObj(testObj), B {
    override fun testCount(): Int {
        return 2
    }

    override fun testB() = age * 5
}

fun TestObj2.getSqAge():Int {
    return age*age
}
interface A {
    fun testA():String = "interface A"
}

interface C {
    fun testA():String = "interface C"
}

interface B{
    fun testB():Int
}